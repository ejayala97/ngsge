# MultiEvent Admin - Descarga y Ejecución en un entorno de desarrollo

Esta aplicación funciona como un administrador de un sistema y debe contar con diferentes funciones que permitan tener un control de los principales puntos del sistema, a continuación, se detallan las principales funcionalidades que permitirá el aplicativo para el administrador.

•	Monitorear Usuarios
•	Administrar Salas
•	Administrar Eventos
•	Administrar Tópicos
•	Generar Reportes de los Eventos

## Clonar repositorio

Abre git en el directorio en el que quieres clonar el repositorio y ejecuta el siguiente comando:

`git clone https://ejayala97@bitbucket.org/ejayala97/ngsge.git` 

## Instala las dependencias usadas en este proyecto

Ejecuta el comando Node en el cmd dentro del directorio raiz:

`npm install`

## Proyecto generado con angular 

Este proyecto es generado con [Angular CLI](https://github.com/angular/angular-cli) versión 8.3.18.

## Development server

Ejecuta `ng serve` para ponerlo en ejecución en `http://localhost:4200/`.

# Desplegar el proyecto con Firebase Hosting

A continuación se resumen un pequeño tutorial para poder desplegar tu proyecto con firebase hosting, se anticipa que tienes un nivel de conocimiento basico acerca de firebase, que tienes una cuenta y que has hecho la respectiva configuración de las credenciales de firebase dentro de tu proyecto.

## Firebase Hosting

Este servicio sirve para poder desplegar la aplicación. Este proyecto fue desplegado en un hosting gratuito en Firebase.

Para usar este servicio es necesario habilitarlo navegando hasta la opción de Hosting en el panel de desarrollo de Firebase y dando clic en “comenzar”.

Las indicaciones que hay que seguir son sencillas para poder usar el hosting de forma efectiva. Lo primero que se debe hacer es instalar el CLI (línea de comandos) de Firebase, para hacer esto se ejecuta en la consola el siguiente comando:

`npm install -g firebase-tools`

Lo siguiente es iniciar sesión en google, ejecutando el comando:

`firebase login`

Una vez que se haya realizado el paso anterior, se inicializa el proyecto con el siguiente comando desde la ruta principal de la aplicación local:

`firebase init`

Selecciona el proyecto, y lo único que resta es ejecutar el siguiente comando:

`firebase deploy`.
