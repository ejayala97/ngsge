import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BeaconService } from '../../../components/beacons/beacon.service';
import { BeaconI } from '../../models/beacon.interface';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { ModalBeaconsComponent } from './../modal-beacons/modal-beacons.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-table-beacons',
  templateUrl: './table-beacons.component.html',
  styleUrls: ['./table-beacons.component.scss']
})
export class TableBeaconsComponent implements OnInit {
  displayedColumns: string[] = ['beaconUID', 'name', 'sala', 'descrip', 'actions'];
  public salas$: Observable<BeaconI[]>;
  public dataSalas: BeaconI[] = [];
  dataSource = new MatTableDataSource();
 
  @ViewChild(MatPaginator, {static:true})paginator:MatPaginator;
  @ViewChild(MatSort, {static: true})sort:MatSort;

  constructor(private beaconSvc: BeaconService, public dialog:MatDialog) { }

  ngOnInit() {
    this.salas$ = this.beaconSvc.getAllBeacons();
    const subs = this.salas$.subscribe(beacons=> {
      beacons.forEach(element => {
        if (element.id != 'DJwmmcQkmNQjAcPsM162') {
          const salaobj = {
            id: element.id,
            beaconUID: element.beaconUID,
            name: element.name,
            sala: element.sala,
            descrip: element.descrip
          }
          this.dataSalas.push(salaobj as BeaconI);
          this.dataSource.data = this.dataSalas;
        }
      });
    }
    //(this.dataSource.data = beacons)
    );
  }
  ngAfterViewInit(){
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  //EDITAR BeaconO
  onEditBeacon(beacon:BeaconI){
    this.dataSalas = [];
    console.log('edit beacon', beacon);
    this.openDialog(beacon);
  }

  //ELIMINAR Beacon
  onDeleteBeacon(beacon:BeaconI){
    console.log('delete beacon', beacon);
    Swal.fire({
      title:'¿Estás seguro de eliminar esta sala?',
      text:'Luego de eliminar no se podra revertir!',
      icon: 'warning',
      heightAuto: false,
      showCancelButton: true,
      confirmButtonColor: '#e63244',
      cancelButtonColor: '#4f9ca5',
      confirmButtonText: 'Confirmar'
    }).then(result => {
      if(result.value){
        this.dataSalas = [];
        //Borrar
        this.beaconSvc.deleteBeaconById(beacon).then(()=>{
          Swal.fire({
            title: 'Eliminado!',
            text: 'La sala ha sido eliminada con éxito.',
            icon: 'success',
            showConfirmButton: false,
            timer: 1500,
            heightAuto: false
          });
        }).catch((error)=> {
          Swal.fire('Error!', '¡Ha ocurrido un error al eliminar el beacon!', 'error');
        });
      }
    })
  }
  onNewBeacon(){
    this.dataSalas = [];
    this.openDialog();
  }

  openDialog(beacon?: BeaconI): void{
    const config = {
      data:{
        message: beacon ? 'Editar Beacon' : 'Nueva Sala',
        content: beacon
      }
    };
    const dialogRef = this.dialog.open(ModalBeaconsComponent, config);
    dialogRef.afterClosed().subscribe(result=>{
      this.dataSalas = [];
      console.log(`Dialog result ${result}`);
    });
  }
  
}
