import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public userData$: Observable<firebase.User>;
  public currentImage: string = 'https://www.tuplanweb.com/proyecto/Plantilla/img/user/edwin.jpg';

  constructor(private afAuth: AuthService) { 
    this.userData$ = afAuth.userData$;
  }

  ngOnInit() {
    this.userData$.subscribe(user=>{
      this.currentImage = user.photoURL
    });
  }

}
