import { Injectable } from '@angular/core';
import { UserI } from '../models/user.interface';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { FileI } from '../models/file.interface';
import { Router } from '@angular/router';
import { UserService } from 'src/app/components/users/user.service';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public userData$: Observable<firebase.User>;
  public userAdmin$: Observable<boolean>;
  private filePath: string;

  constructor(private afAuth: AngularFireAuth, private route:Router,private storage:AngularFireStorage, private router: Router, private userSvc: UserService) { 
    this.userData$ = afAuth.authState;
  }

  loginByEmail(user:UserI){
    const { email, password } = user;
    this.afAuth.auth.signInWithEmailAndPassword(email, password).then((res:any)=>{

      this.userSvc.getOneUser(res.user.uid).subscribe(us=>{
        if (us.role){
          this.route.navigate(['/home']);
          return;
        }else{
          this.logout();
          Swal.fire({
            icon: 'error',
            title: 'Usted no es administrador!',
            text: 'Esta aplicación es unicamente accesible por un administrador.',
            heightAuto: false
          })
        }
      })
    }).catch(err => {
      Swal.fire({
        icon: 'error',
        title: 'Usuario o contraseña incorrectos!',
        text: 'El correo o la contraseña que ingreso son incorrectos, vuelva a intentarlo.',
        heightAuto: false
      })
    });
  }

  logout(){
    this.afAuth.auth.signOut();
    this.router.navigate(['/login']);
  }
  preSaveProfile(user: UserI, image: FileI, idUser:string): void{
    if(image){
      this.uploadImage(user, image, idUser);
    }else{
      this.saveUserProfile(user, idUser);
    }
    
  }
  private uploadImage(user: UserI, image: FileI, idUser:string): void {
    this.filePath = `images/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);
    task.snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(urlImage => {
            user.photoURL = urlImage;
            this.saveUserProfile(user, idUser);
          });
        })
      ).subscribe();
  }

  private saveUserProfile(user:UserI, idUser:string){
    this.userSvc.updateUserProfile(user, idUser);
    this.afAuth.auth.currentUser.updateProfile({
      displayName: user.displayName,
      photoURL: user.photoURL
    })
    .then(()=> console.log('updated'))
    .catch(err => console.log('Error', err));
  }
}
