export interface EventI{
	id?: string;
	title: string;
	siglas: string;
	descrip: string;
	topics: string;
	date?: any;
	finishdate?: any;
	time: any;
	typeEv?: string;
	platform?: string;
	sala?: string;
	idsala?: string;
	idSubevents?: any;
	idUsers?: any;
}