export interface UserI{
	email: string;
	password?: string;
	displayName?: string;
	name?: string;
	lastname?: string;
	role?: string;
	photoURL?: string;
	uid?: string;
	phoneNumber?: string;
	description?: string;
}