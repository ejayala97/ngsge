import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { EventI } from '../../../shared/models/event.interface';
import { EventService } from './../../events/event.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { DashboardService } from './dashboard.service';
import { UserService } from '../../users/user.service';
import { UserI } from 'src/app/shared/models/user.interface';
import { element } from 'protractor';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public events$: Observable<EventI[]>;
  public users$: Observable<UserI[]>;
  public numsubs: any = 0;
  cards = [];
  dataevents = [];
  pieChart = [];
  band: boolean = false;

  public dataPie: any[] = [];
  public userList: UserI[] = [];

  displayedColumns: string[] = ['num', 'name', 'email'];
  dataSource = new MatTableDataSource();
  public num: any = 0;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private eventSvc: EventService, private dashboardService: DashboardService, private usersSvc: UserService) { }
  ngOnInit() {
    this.events$ = this.eventSvc.getAllEvents();
    this.users$ = this.usersSvc.getAllUsers();
    this.cards = this.dashboardService.cards();
    //Subscribe un observable para poder asignar la data al Pie
    this.events$.subscribe(events => {
      console.log(events.length);
      events.forEach(element => {
        if(element.idUsers){
          var obj= {
            name: element.siglas,
            y: element.idUsers.length,
            selected: true
          }
          this.pieChart.push(obj);
          this.numsubs = element.idSubevents.length + this.numsubs;
        }else{
          var obj= {
            name: element.siglas,
            y: this.num,
            selected: true
          }
          this.pieChart.push(obj); 
          this.numsubs = element.idSubevents.length + this.numsubs;   
        }
      })
    })
    this.band = true;
    this.dataSource.paginator = this.paginator;
    this.users$.subscribe(users => {
      users.forEach(user => {
        if(!user.role){
          const ObjUser = {
            email: user.email,
            //lastname: user.lastname,
            displayName: user.displayName,
            //name: user.name,
            uid: user.uid
          }
          this.userList.push(ObjUser as UserI);
        }
        this.dataSource.data = this.userList;
        //subscribe.unsubscribe();
      })
    })
  }
}
