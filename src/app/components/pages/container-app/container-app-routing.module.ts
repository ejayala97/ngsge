import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './../../../shared/guards/auth.guard';
import { ContainerAppComponent } from './container-app.component';
import { redirectUnauthorizedTo, AngularFireAuthGuard } from '@angular/fire/auth-guard';
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
  { path: '', component: ContainerAppComponent,
    //canActivate: [AuthGuard],
    canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin },
    children:[
      {
        path:'', redirectTo:'/', pathMatch: 'full'
      },
      {
        path: 'events',
        loadChildren: ()=>
          import('../../events/list-events/list-events.module').then(
            m=> m.ListEventsModule
          )
      },
      { 
        path: 'beacons', 
        loadChildren: () => 
          import('../../beacons/list-beacons/list-beacons.module').then(
            m => m.ListBeaconsModule) 
      },
      { 
        path: 'users', 
        loadChildren: () => 
        import('../../users/list-users/list-users.module').then
        (m => m.ListUsersModule) 
      }
    ]
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class ContainerAppRoutingModule { }
