import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TopicService } from '../topic.service';
import Swal from 'sweetalert2';
import { TopicI } from 'src/app/shared/models/topic.interface';

@Component({
  selector: 'app-new-topic',
  templateUrl: './new-topic.component.html',
  styleUrls: ['./new-topic.component.scss']
})
export class NewTopicComponent implements OnInit {
  
  constructor(public dialog: MatDialogRef<NewTopicComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private topicsSvc: TopicService) { }

  public newTopicForm = new FormGroup({
      title: new FormControl('', Validators.required)
    })

  ngOnInit() {
    console.log('llego al componnt')
  }
  addNewTopic(data: TopicI) {
    console.log('Nuevo evento', data);
    const result = this.topicsSvc.saveTopic(data).then(() => {
      Swal.fire({
        title: 'Registrado!',
        text: 'El evento ha sido registrado exitosamente!',
        icon: 'success',
        showConfirmButton: false,
        timer: 1500,
        heightAuto: false
      });
    }).catch((error) => {
      Swal.fire('Error!', '¡Ha ocurrido un error al añadir el evento!', 'error');
    })
  }
}
