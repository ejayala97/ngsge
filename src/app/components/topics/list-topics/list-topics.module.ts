import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../material.module';
import { ListTopicsRoutingModule } from './list-topics-routing.module';
import { ListTopicsComponent } from './list-topics.component';
import { NewTopicComponent } from '../new-topic/new-topic.component';


@NgModule({
  declarations: [ListTopicsComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ListTopicsRoutingModule
  ]
})
export class ListTopicsModule { }
