import { Component, OnInit, ViewChild } from '@angular/core';
import { TopicService } from '../topic.service';
import { Observable } from 'rxjs';
import { TopicI } from 'src/app/shared/models/topic.interface';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { NewTopicComponent } from '../new-topic/new-topic.component';
import { EditTopicComponent } from '../edit-topic/edit-topic.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-topics',
  templateUrl: './list-topics.component.html',
  styleUrls: ['./list-topics.component.scss']
})
export class ListTopicsComponent implements OnInit {
  displayedColumns: string[] = ['num', 'name', 'actions'];
  public topics$: Observable<TopicI[]>
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private topicsSvc: TopicService, public dialog: MatDialog) { }

  ngOnInit() {
    this.topics$ = this.topicsSvc.getAllTopics();
    this.topics$.subscribe(topics => (this.dataSource.data = topics));
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(beacon: Event) {
    const filterValue = (beacon.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  onNewTopic() {
    console.log('llego aca');
    this.openDialog();
  }
  onEditTopic(topic: TopicI) {
    const editEv = true;
    this.openDialog(topic, editEv);
  }
  openDialog(topic?: TopicI, edit?: boolean): void {
    var config;
    if (edit) {
      console.log("Entro aqui");
      config = {
        data: {
          message: 'Editar Topico',
          content: topic
        }
      };
      //se agrega la variable sub para controlar que se ingresara un nuevo suevento.
      const dialogRef = this.dialog.open(EditTopicComponent, config);
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result ${result}`);
      });
    } else {
      config = {
        data: {
          message: 'Nuevo Tópico'
        }
      };
      const dialogRef = this.dialog.open(NewTopicComponent, config);
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result ${result}`);
      });
    }
  }

  //ELIMINAR Beacon
  onDeleteBeacon(topic: TopicI) {
    console.log('delete beacon', topic);
    Swal.fire({
      title: '¿Estás seguro de eliminar este topico?',
      text: 'Luego de eliminar no se podra revertir!',
      icon: 'warning',
      heightAuto: false,
      showCancelButton: true,
      confirmButtonColor: '#e63244',
      cancelButtonColor: '#4f9ca5',
      confirmButtonText: 'Confirmar'
    }).then(result => {
      if (result.value) {
        //Borrar
        this.topicsSvc.deleteEventById(topic.id).then(() => {
          Swal.fire({
            title: 'Eliminado!',
            text: 'El topico se eliminó con éxito.',
            icon: 'success',
            showConfirmButton: false,
            timer: 1500,
            heightAuto: false
          });
        }).catch((error) => {
          Swal.fire('Error!', '¡Ha ocurrido un error al eliminar el beacon!', 'error');
        });
      }
    })
  }
}
