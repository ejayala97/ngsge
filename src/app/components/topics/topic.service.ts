import { Injectable } from '@angular/core';
import { TopicI } from 'src/app/shared/models/topic.interface';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TopicService {
  private topicsCollection: AngularFirestoreCollection<TopicI>;
  public topicObj: TopicI;
  constructor(private afs: AngularFirestore) { 
    this.topicsCollection = afs.collection<TopicI>('topics');
  }

  public getAllTopics(): Observable<TopicI[]> {
    return this.topicsCollection
      .snapshotChanges()
      .pipe(
        map(actions =>
          actions.map(a => {
            const data = a.payload.doc.data() as TopicI;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      )
  }
  public saveTopic(topico: TopicI){
    console.log('objeto: ', topico);
    this.topicObj = {
      title: topico.title
    }
    if (topico.id) {
      console.log('ENTRO A EDITAR');
      return this.topicsCollection.doc(topico.id).update(this.topicObj);
    } else {
      //Add cocument without id
      const newEv = this.topicsCollection.add(this.topicObj);
      //set id document with data 
      const prom = newEv.then(res =>{
        this.topicsCollection.doc(res.id).set({
          id: res.id
        },{ merge: true })
      })
      return prom;
    }
  }

  public deleteEventById(id: string) {
    return this.topicsCollection.doc(id).delete();
  }
}
