import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TopicService } from '../topic.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { TopicI } from 'src/app/shared/models/topic.interface';

@Component({
  selector: 'app-edit-topic',
  templateUrl: './edit-topic.component.html',
  styleUrls: ['./edit-topic.component.scss']
})
export class EditTopicComponent implements OnInit {

  constructor(public dialog: MatDialogRef<EditTopicComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private topicsSvc: TopicService) { }

  public editTopicForm = new FormGroup({
    id: new FormControl('', Validators.required),
    title: new FormControl('', Validators.required)
  })

  ngOnInit() {
    console.log(this.data.content.id)
    this.initValuesForm();
  }

  editTopic(topic: TopicI) {
    this.topicsSvc.saveTopic(topic).then(() => {
      Swal.fire({
        title: 'Guardado!',
        text: 'Se han guardo los cambios exitosamente!',
        icon: 'success',
        showConfirmButton: false,
        timer: 1500,
        heightAuto: false
      });
    }).catch((error) => {
      Swal.fire('Error!', '¡Ha ocurrido un error al editar este topico!', 'error');
    });
  }

  private initValuesForm(): void {
    this.editTopicForm.patchValue({
      id: this.data.content.id,
      title: this.data.content.title
    })
  }
}
