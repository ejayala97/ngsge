import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { EventI } from '../../../shared/models/event.interface';
import { EventService } from '../event.service';
import { BeaconService } from '../../beacons/beacon.service';
import { Observable } from 'rxjs';
import { BeaconI } from 'src/app/shared/models/beacon.interface';
import Swal from 'sweetalert2';
import { TopicService } from '../../topics/topic.service';
import { TopicI } from 'src/app/shared/models/topic.interface';

@Component({
  selector: 'app-new-subevent',
  templateUrl: './new-subevent.component.html',
  styleUrls: ['./new-subevent.component.scss']
})
export class NewSubeventComponent implements OnInit {
  public salas$: Observable<BeaconI[]>;
  beaconControl = new FormControl('', Validators.required);
  Listbeacons: BeaconI[] = [];
  selectedOP: string = 'Presencial';
  public topics$: Observable<TopicI[]>
  listTopics: TopicI[] = [];
  
  constructor(private eventSvc: EventService, private beaconSvc: BeaconService, private topicsSvc: TopicService) { }
  @Input() event: EventI;
  //@Input() sub: String;

  public newEventForm = new FormGroup({
    title: new FormControl('', Validators.required),
    siglas: new FormControl('', Validators.required),
    descrip: new FormControl('', Validators.required),
    typeEv: new FormControl('', Validators.required),
    platform: new FormControl('', Validators.required),
    sala: new FormControl('', Validators.required),
    date: new FormControl('', Validators.required),
    finishdate: new FormControl('', Validators.required),
    time: new FormControl('', Validators.required),
    topics: new FormControl('', Validators.required)
  })


  ngOnInit() {
    this.Listbeacons = [];
    this.getTopics();
    this.getsalas();
  }

  addNewSubEvent(data: EventI) {
    try {
      this.eventSvc.saveSubEvent(data, this.event).then(() =>{
        Swal.fire({
          title: 'Registrado!',
          text: 'El evento ha sido registrado exitosamente!',
          icon: 'success',
          showConfirmButton: false,
          timer: 1500,
          heightAuto: false 
        });
      })
    } catch (error) {
      Swal.fire({
        title: 'Error!',
        text: 'Ha ocurrido un error al registrar este subevento, asegurate que los datos que ingreses no se pinten de rojo.',
        icon: 'error',
        showConfirmButton: true,
        heightAuto: false
      });
    }
  }

  public getsalas() {
    this.salas$ = this.beaconSvc.getAllBeacons();
    this.salas$
      .subscribe(beacon => {
        beacon.forEach(element => {
          if (element.id != 'DJwmmcQkmNQjAcPsM162') {
            const beaconObj = {
              id: element.id,
              name: element.name,
              sala: element.sala,
              descrip: element.descrip
            };
            this.Listbeacons.push(beaconObj as BeaconI);
          }
        })
      })
  }

  getTopics(){
    this.topics$ = this.topicsSvc.getAllTopics();
    this.topics$.subscribe(topics => {
      topics.forEach(element => {
        this.listTopics.push(element as TopicI)
      });
    });
  }

  resetForm(newEventForm: NgForm) {
    if (newEventForm != null)
      newEventForm.reset();
  }

}
