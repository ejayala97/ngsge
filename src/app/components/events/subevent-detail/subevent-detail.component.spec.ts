import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubeventDetailComponent } from './subevent-detail.component';

describe('SubeventDetailComponent', () => {
  let component: SubeventDetailComponent;
  let fixture: ComponentFixture<SubeventDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubeventDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubeventDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
