import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { EventI } from 'src/app/shared/models/event.interface';
import { EventService } from '../event.service';
import { BeaconService } from '../../beacons/beacon.service';
import { BeaconI } from 'src/app/shared/models/beacon.interface';
import { UserI } from 'src/app/shared/models/user.interface';
import { UserService } from '../../users/user.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { ModalComponent } from './../../../shared/components/modal/modal.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-subevent-detail',
  templateUrl: './subevent-detail.component.html',
  styleUrls: ['./subevent-detail.component.scss']
})
export class SubeventDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private evtSvc: EventService, public dialog: MatDialog, private beaSvc: BeaconService, private Usersvc: UserService) { }

  public idSub: any;
  public subevent$: Observable<EventI>;
  public sala$: Observable<BeaconI>;
  public subs: Subscription;
  public users$: Observable<UserI[]>;
  public user$: Observable<UserI>;
  public userList: UserI[] = [];
  public Subevento: EventI;
  displayedColumnsUsers: string[] = ['name', 'email', 'actions'];
  dataSourceUsers = new MatTableDataSource();

  ngOnInit() {
    this.idSub = this.route.snapshot.params.id;
    this.subevent$ = this.evtSvc.getOneSubEvent(this.idSub);
    this.subs = this.subevent$.subscribe(sub => {
      //console.log(sub);
      this.Subevento =sub;
      this.sala$ = this.beaSvc.getBeacon(sub.sala);
      const subssala = this.sala$.subscribe();
      subssala.unsubscribe();
    });
    this.loadPaticipants();
  }

  ngOnDestroy() {
    console.log("Entro a desuscribir");
    this.subs.unsubscribe();
  }

  loadPaticipants() {
    const subs = this.subevent$.subscribe(evt => {
      if (evt.idUsers) {
        evt.idUsers.forEach(iduser => {
          this.user$ = this.Usersvc.getOneUser(iduser);
          this.user$.subscribe(us => {
            console.log(us.uid);
            const ObjUser = {
              uid: us.uid,
              email: us.email,
              //lastname: us.lastname,
              displayName: us.displayName
              //name: us.name,
            }
            this.userList.push(ObjUser as UserI);
            console.log(ObjUser);
            this.dataSourceUsers.data = this.userList;
            subs.unsubscribe();
          })
        });
      } else {
        console.log('No hay usuarios');
      }
    })
  }
  onEditsubEvent() {
    const editEv = true;
    this.openDialog(this.Subevento, editEv);
  }
  openDialog(event?: EventI, edit?: boolean): void {
    var config;
    if (edit) {
      config = {
        data: {
          message: event ? 'Editar Subevento' : 'Nuevo Subevento',
          content: event,
          edit: 'true'
        }
      };
    } else {
      config = {
        data: {
          message: event ? 'Crear Subevento' : 'Nuevo Subevento',
          content: event,
          sub: 'true'
        }
      };
    }
    //se agrega la variable sub para controlar que se ingresara un nuevo suevento.
    const dialogRef = this.dialog.open(ModalComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result ${result}`);
/*       if (result) {
        console.log('Entro a este result');
        this.loadsubEvents();
      } */
    });
  }

    //Dropea un usario del evento
    reomveUser(uid: string) {
      //console.log('delete event', uid);
      Swal.fire({
        title: '¿Estás seguro de remover este usuario?',
        icon: 'warning',
        heightAuto: false,
        showCancelButton: true,
        confirmButtonColor: '#e63244',
        cancelButtonColor: '#4f9ca5',
        confirmButtonText: 'Confirmar'
      }).then(result => {
        if (result.value) {
          this.evtSvc.removeUserfromSubEvent(uid, this.idSub).then(() => {
            //Borrar
            this.userList = [];
            this.loadPaticipants();
            Swal.fire({
              title: 'Eliminado!',
              text: 'El usuario ha sido eliminado de este evento.',
              icon: 'success',
              heightAuto: false
            });
          }).catch((error) => {
            Swal.fire('Error!', '¡Ha ocurrido un error al eliminar el evento!', 'error');
          });
        }
      })
    }
}
