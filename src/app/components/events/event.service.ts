import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EventI } from '../../shared/models/event.interface';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { platform } from 'os';
import { Action } from 'rxjs/internal/scheduler/Action';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  private eventsCollection: AngularFirestoreCollection<EventI>;
  private subeventsCollection: AngularFirestoreCollection<EventI>;

  events: EventI[] = [];

  public eventoObj: EventI;
  public SubeventObj: EventI;
  constructor(private afs: AngularFirestore) {
    this.eventsCollection = afs.collection<EventI>('events');
    this.subeventsCollection = afs.collection<EventI>('subevents');
  }

  public getAllEvents(): Observable<EventI[]> {
    return this.eventsCollection
      .snapshotChanges()
      .pipe(
        map(actions =>
          actions.map(a => {
            const data = a.payload.doc.data() as EventI;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      )
  }

  public getOneEvent(id: EventI): Observable<EventI> {
    return this.afs.doc<EventI>(`events/${id}`).valueChanges();
  }

  public getOneSubEvent(id: string): Observable<EventI> {
    return this.afs.doc<EventI>(`subevents/${id}`).valueChanges();
  }

  public deleteEventById(event: EventI) {
    if (typeof event.idSubevents === 'undefined' || event.idSubevents == 0) {
      // variable is undefined or null
      console.log("No subeventos");
      return this.eventsCollection.doc(event.id).delete();
    } else {
      console.log("Si hay subeventos");
      event.idSubevents.forEach(element => {
        console.log("Eliminando evento: ", element);
        this.subeventsCollection.doc(element).delete();
      });
      return this.eventsCollection.doc(event.id).delete();
    }
  }

  public editEventById(event: EventI) {
    return this.eventsCollection.doc(event.id).update(event);
  }

  public saveEvent(event: EventI) {
    const date = this.formatDate(event.date);
    const finishdateObj = this.formatDate(event.finishdate);
    if (event.typeEv === 'Presencial') {
      this.eventoObj = {
        title: event.title,
        siglas: event.siglas,
        descrip: event.descrip,
        topics: event.topics,
        date: date,
        typeEv: event.typeEv,
        idSubevents: "",
        finishdate: finishdateObj,
        time: event.time,
        sala: event.sala
      };
    } else if (event.typeEv === 'En Linea') {
      this.eventoObj = {
        title: event.title,
        siglas: event.siglas,
        descrip: event.descrip,
        topics: event.topics,
        date: date,
        typeEv: event.typeEv,
        idSubevents: "",
        sala: 'DJwmmcQkmNQjAcPsM162',
        finishdate: finishdateObj,
        time: event.time,
        platform: event.platform
      };
    }
    if (event.id) {
      return this.eventsCollection.doc(event.id).update(this.eventoObj);
    } else {
      //Add cocument without id
      const newEv = this.eventsCollection.add(this.eventoObj);
      console.log(newEv);
      //set id document with data 
      const prom = newEv.then(res => {
        this.eventsCollection.doc(res.id).set({
          id: res.id
        }, { merge: true })
      })
      return prom;
    }
  }

  public saveSubEvent(subevent: EventI, event?: EventI) {
    var idsub: string;
    const date = this.formatDate(subevent.date);
    const finishdateObj = this.formatDate(subevent.finishdate);
    console.log(subevent);
    if (subevent.typeEv === 'Presencial') {
      this.SubeventObj = {
        title: subevent.title,
        siglas: subevent.siglas,
        descrip: subevent.descrip,
        topics: subevent.topics,
        date: date,
        finishdate: finishdateObj,
        typeEv: subevent.typeEv,
        time: subevent.time,
        sala: subevent.sala
      };
    } else if (subevent.typeEv === 'En Linea') {
      this.SubeventObj = {
        title: subevent.title,
        siglas: subevent.siglas,
        descrip: subevent.descrip,
        topics: subevent.topics,
        date: date,
        finishdate: finishdateObj,
        typeEv: subevent.typeEv,
        time: subevent.time,
        sala: 'DJwmmcQkmNQjAcPsM162',
        platform: subevent.platform
      };
    }
    if (subevent.id) {
      return this.subeventsCollection.doc(subevent.id).update(this.SubeventObj);
    } else {
      // añade el nuevo subevento a firestore
      var newSUb = this.subeventsCollection.add(this.SubeventObj);

      const promval = newSUb.then(res => {
        idsub = res.id;
        const eventRef = this.afs.collection("events").doc(event.id);
        // Agrega automaticamente la referencia de un subevento en el evento master.
        eventRef.update({
          idSubevents: firebase.firestore.FieldValue.arrayUnion(idsub)
        });
        //añade el id al nuevo doc
        if (subevent.typeEv === 'Presencial') {
          this.SubeventObj = {
            id: idsub,
            title: subevent.title,
            siglas: subevent.siglas,
            descrip: subevent.descrip,
            topics: subevent.topics,
            date: date,
            finishdate: finishdateObj,
            typeEv: subevent.typeEv,
            time: subevent.time,
            sala: subevent.sala
          };
        } else if (subevent.typeEv === 'En Linea') {
          this.SubeventObj = {
            id: idsub,
            title: subevent.title,
            siglas: subevent.siglas,
            descrip: subevent.descrip,
            topics: subevent.topics,
            date: date,
            finishdate: finishdateObj,
            typeEv: subevent.typeEv,
            time: subevent.time,
            sala: 'DJwmmcQkmNQjAcPsM162',
            platform: subevent.platform
          };
        }
        this.subeventsCollection.doc(idsub).set(this.SubeventObj)
      })
      return promval;
    }

  }

  //remueve un usuario de un evento 
  public removeUserfromEvent(user: string, idEvt: string) {
    const eventRef = this.afs.collection("events").doc(idEvt);
    var valor = eventRef.update({
      idUsers: firebase.firestore.FieldValue.arrayRemove(user)
    });
    return valor;
  }

  public removeUserfromSubEvent(user: string, idEvt: string) {
    const eventRef = this.afs.collection("subevents").doc(idEvt);
    var valor = eventRef.update({
      idUsers: firebase.firestore.FieldValue.arrayRemove(user)
    });
    return valor;
  }


  //borra un subevento
  public deletesubEventById(eventid: string, idsub: string) {
    const val = this.subeventsCollection.doc(idsub).delete();
    const eventRef = this.afs.collection("events").doc(eventid);
    //Elimina la refrencia del subevento en el evento master
    eventRef.update({
      idSubevents: firebase.firestore.FieldValue.arrayRemove(idsub)
    });

    return val;
  }
  formatDate(date: Date): string {
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();

    return `${month}/${day}/${year}`;
  }
}
