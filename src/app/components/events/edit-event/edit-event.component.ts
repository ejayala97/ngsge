import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { EventI } from '../../../shared/models/event.interface';
import { EventService } from '../event.service';
import { BeaconI } from 'src/app/shared/models/beacon.interface';
import { Observable } from 'rxjs';
import { BeaconService } from '../../beacons/beacon.service';
import Swal from 'sweetalert2';
import { TopicI } from 'src/app/shared/models/topic.interface';
import { TopicService } from '../../topics/topic.service';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.scss']
})
export class EditEventComponent implements OnInit {
  public salas$: Observable<BeaconI[]>;
  public plat$: Observable<string>;
  beacons: BeaconI[] = [];
  selectedOP: string;
  topicos = new FormControl('', Validators.required);
  topicosList: string[] = [
    'Animación y simulación',
    'Segmentación y agrupación en imágenes y videos',
    'Codificación, compresión y transmisión de imágenes/videos',
    'Captura, edición y síntesis de movimiento',
    'Realismo y síntesis de imágenes',
    'Técnicas y algoritmos de rendering',
    'Hardware para computación gráfica',
    'Realidad virtual, aumentada y mixta',
    'Interfaces para realidad virtual',
    'Procesamiento de imágenes y videos',
    'Detección y reconocimiento de características'
  ];
  public topics$: Observable<TopicI[]>
  listTopics: TopicI[] = [];
  @Input() event: EventI;
  @Input() edit?: boolean;
  constructor(private eventSvc: EventService, private beaconSvc: BeaconService, private topicsSvc: TopicService) { }

  public editEventForm = new FormGroup({
    id: new FormControl('', Validators.required),
    title: new FormControl('', Validators.required),
    siglas: new FormControl('', Validators.required),
    sala: new FormControl('', Validators.required),
    descrip: new FormControl('', Validators.required),
    date: new FormControl('', Validators.required),
    finishdate: new FormControl('', Validators.required),
    time: new FormControl('', Validators.required),
    typeEv: new FormControl('', Validators.required),
    platform: new FormControl('', Validators.required),
    topics: this.topicos,
  })
  ngOnInit() {
    this.selectedOP = this.event.typeEv;
    this.getTopics();
    this.initValuesForm();
    this.salas$ = this.beaconSvc.getAllBeacons();
    this.salas$
      .subscribe(beacon => {
        beacon.forEach(element => {
          if (element.id != 'DJwmmcQkmNQjAcPsM162') {
            const beaconObj = {
              id: element.id,
              name: element.name,
              sala: element.sala,
              descrip: element.descrip
            };
            this.beacons.push(beaconObj as BeaconI);
          }
        }
        )
      });
  }
  editEvent(event: EventI) {
    console.log(event);
    if (this.edit) {
      this.eventSvc.saveSubEvent(event).then(() => {
        Swal.fire({
          title: 'Cambios en el evento Guardados!',
          text: 'Se han guardo los cambios del subevento exitosamente!',
          icon: 'success',
          showConfirmButton: false,
          timer: 1500,
          heightAuto: false
        });
      }).catch((error) => {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Este subevento no se pudo editar!',
          heightAuto: false
        })
      })
    } else {
      try {
        this.eventSvc.saveEvent(event).then(() => {
          Swal.fire({
            title: 'Cambios en el evento Guardados!',
            text: 'Se han guardo los cambios exitosamente!',
            icon: 'success',
            showConfirmButton: false,
            timer: 1500,
            heightAuto: false
          });
        })
      } catch (error) {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Este evento no se pudo editar, asegurate de que los datos esten correctos.',
          heightAuto: false
        })
      }
    }
  }

  private initValuesForm(): void {
    console.log(this.event.typeEv, ' y ', this.event.platform)
    this.editEventForm.patchValue({
      id: this.event.id,
      title: this.event.title,
      siglas: this.event.siglas,
      sala: this.event.idsala,
      descrip: this.event.descrip,
      typeEv: this.event.typeEv,
      platform: this.event.platform,
      date: this.formatDate(this.event.date),
      finishdate: this.formatDate(this.event.finishdate),
      time: this.event.time,
      topics: this.event.topics
    })
  }
  resetForm(editEventForm: NgForm) {
    if (editEventForm != null)
      editEventForm.reset();
  }

  getTopics() {
    this.topics$ = this.topicsSvc.getAllTopics();
    this.topics$.subscribe(topics => {
      topics.forEach(element => {
        this.listTopics.push(element as TopicI)
      });
    });
    console.log(this.listTopics);
  }
  formatDate(selectDate: String): Date {
    var splitted = selectDate.split("/");
    var newSelectDate = new Date();
    var mes = parseInt(splitted[0]);
    newSelectDate.setMonth(mes - 1);
    newSelectDate.setDate(+splitted[1]);
    newSelectDate.setFullYear(+splitted[2]);

    return newSelectDate;
  }
}
