import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { EventService } from '../../events/event.service';
import { EventI } from '../../../shared/models/event.interface';
import { Observable } from 'rxjs';
import { MatAccordion } from '@angular/material';
import { element } from 'protractor';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  //public events$: Observable<EventI[]>;
  @Input() event: EventI;
  @ViewChild(MatAccordion, { static: true }) accordion: MatAccordion;

  public subeventList: EventI[] = [];
  public idsSubs: string[] = [];
  public evento$: Observable<EventI>;
  public subevento$: Observable<EventI>;
  constructor(private eventSvc: EventService) { }

  ngOnInit() {
    // this.eventSvc.getAllEvents().subscribe(res => console.log('EVENTOS', res));
    //this.evento$ = this.eventSvc.getOneEvent(this.event);
    /*this.evento$.subscribe(evt =>{
      console.log('evt', evt);
    });*/
    if (typeof this.event.idSubevents === 'undefined' || this.event.idSubevents == 0) {
      // variable is undefined or null
      console.log('no hay subeventos');
    } else {
      console.log('id', this.event.id);
      this.idsSubs = this.event.idSubevents;
      this.idsSubs.forEach(element => {
        this.subevento$ = this.eventSvc.getOneSubEvent(element);
        this.subevento$.subscribe(sub => {
          console.log('sub', sub.id);
          this.subeventList.push(sub as EventI);
        });
      });
    }
  }
}
