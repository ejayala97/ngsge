import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventService } from '../event.service';
import { Observable, Subscription } from 'rxjs';
import { EventI } from '../../../shared/models/event.interface';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from './../../../shared/components/modal/modal.component';
import Swal from 'sweetalert2';
import { BeaconService } from '../../beacons/beacon.service';
import { BeaconI } from 'src/app/shared/models/beacon.interface';
import { format } from 'url';
//import { jsPDF } from "jspdf";
declare var jsPDF: any;
import autoTable from 'jspdf-autotable'
import { UserI } from 'src/app/shared/models/user.interface';
import { UserService } from '../../users/user.service';
//import * as jsPDF from 'jspdf'; 

@Component({
  selector: 'app-details-event',
  templateUrl: './details-event.component.html',
  styleUrls: ['./details-event.component.scss']
})
export class DetailsEventComponent implements OnInit {
  public event$: Observable<EventI>;
  //public subids: Array<string>;
  public subevent$: Observable<EventI>;
  public subevents$: Observable<EventI[]>;
  public subeventList: EventI[] = [];
  public beacon$: Observable<BeaconI>;
  public idEvent: any;
  displayedColumns: string[] = ['title', 'actions'];
  dataSource = new MatTableDataSource();
  //users
  public users$: Observable<UserI[]>;
  public user$: Observable<UserI>;
  public userList: UserI[] = [];
  public idsSubs: string[] = [];
  displayedColumnsUsers: string[] = ['name','email', 'actions'];
  dataSourceUsers = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private route: ActivatedRoute, private beaconSvc: BeaconService, private eventSvc: EventService, public dialog: MatDialog, private Usersvc: UserService) { }
  public subscription: Subscription;
  public subscripSub: Subscription;

  ngOnInit() {
    this.idEvent = this.route.snapshot.params.id;
    this.event$ = this.eventSvc.getOneEvent(this.idEvent);
    this.subscription = this.event$.subscribe(event => {
      console.log('Sala de master', event.sala);
      if (typeof event.idSubevents === 'undefined' || event.idSubevents == 0) {
        // variable is undefined or null
        console.log('no hay subeventos');
      } else {
        this.idsSubs = event.idSubevents;
        this.loadsubEvents();
      }
    });

    this.loadPaticipants();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onEditsubEvent(subevent: EventI) {
    const editEv = true;
    this.openDialog(subevent, editEv);
  }

  /*envia datos del evento master para guardar su referencia posteriormente
  Registra un subevento*/
  onEditEvent(event: EventI) {
    this.openDialog(event);
  }

  openDialog(event?: EventI, edit?: boolean): void {
    var config;
    if (edit) {
      config = {
        data: {
          message: event ? 'Editar Subevento' : 'Nuevo Subevento',
          content: event,
          edit: 'true'
        }
      };
    } else {
      config = {
        data: {
          message: event ? 'Crear Subevento' : 'Nuevo Subevento',
          content: event,
          sub: 'true'
        }
      };
    }
    //se agrega la variable sub para controlar que se ingresara un nuevo suevento.
    const dialogRef = this.dialog.open(ModalComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result ${result}`);
      if (result) {
        console.log('Entro a este result');
        this.loadsubEvents();
      }
    });
  }

  loadPaticipants() {
    const subs = this.event$.subscribe(evt => {
      if (evt.idUsers) {
        evt.idUsers.forEach(iduser => {
          this.user$ = this.Usersvc.getOneUser(iduser);
          this.user$.subscribe(us => {
            const ObjUser = {
              idUser: us.uid,
              email: us.email,
              displayName: us.displayName,
              lastname: us.lastname,
              name: us.name,
              uid: us.uid
            }
            this.userList.push(ObjUser as UserI);
            this.dataSourceUsers.data = this.userList;
            subs.unsubscribe();
          })
        });
      } else {
        console.log('No hay usuarios');
      }
    })
  }

  loadsubEvents() {
    this.subeventList = [];
    this.idsSubs.forEach(element => {
      this.subevent$ = this.eventSvc.getOneSubEvent(element);
      this.subscripSub = this.subevent$.subscribe(subevent => {
        if (typeof subevent.sala != 'undefined') {
          this.beacon$ = this.beaconSvc.getBeacon(subevent.sala);
          const subscriptionsala = this.beacon$.subscribe(res => {
            console.log('entro aqui', res.sala);
            const subeventObj = {
              id: subevent.id,
              title: subevent.title,
              siglas: subevent.siglas,
              descrip: subevent.descrip,
              topics: subevent.topics,
              date: subevent.date,
              finishdate: subevent.finishdate,
              time: subevent.time,
              typeEv: subevent.typeEv,
              platform: subevent.platform,
              idsala: subevent.sala,
              sala: res.sala
            };
            //console.log('sala: ', subeventObj.sala, 'idsala:', subeventObj.idsala);
            this.subeventList.push(subeventObj as EventI);
            this.dataSource.data = this.subeventList;
            subscriptionsala.unsubscribe();
            this.subscripSub.unsubscribe();
          })
        }else{
          console.log('Entro aca');
        }
      })
    });
    console.log('subeventList', this.subeventList)
  }
  //ELIMINAR EVENTO
  onDeletesubEvent(subevent: EventI) {
    console.log(subevent);
    try {
      Swal.fire({
        title: '¿Estás seguro de eliminar este Evento?',
        text: 'Luego de eliminar no se podra revertir!',
        icon: 'warning',
        heightAuto: false,
        showCancelButton: true,
        confirmButtonColor: '#e63244',
        cancelButtonColor: '#4f9ca5',
        confirmButtonText: 'Confirmar'
      }).then(result => {
        if (result.value) {
          //Borrar
          //this.subeventList = [];
          this.eventSvc.deletesubEventById(this.idEvent, subevent.id).then(() => {
            //this.loadsubEvents();
            Swal.fire({
              title: 'Eliminado!',
              text: 'El evento ha sido eliminado con éxito.',
              icon: 'success',
              heightAuto: false
            });
          }).catch((error) => {
            Swal.fire('Error!', '¡Ha ocurrido un error al eliminar el evento!', 'error');
          });
        }
      })
    } catch (error) {
      console.log('Un error controlado');
    }
  }

  //Dropea un usario del evento
  reomveUser(uid: string) {
    //console.log('delete event', uid);
    Swal.fire({
      title: '¿Estás seguro de remover este usuario?',
      icon: 'warning',
      heightAuto: false,
      showCancelButton: true,
      confirmButtonColor: '#e63244',
      cancelButtonColor: '#4f9ca5',
      confirmButtonText: 'Confirmar'
    }).then(result => {
      if (result.value) {
        this.eventSvc.removeUserfromEvent(uid, this.idEvent).then(() => {
          //Borrar
          this.userList = [];
          this.loadPaticipants();
          Swal.fire({
            title: 'Eliminado!',
            text: 'El usuario ha sido eliminado de este evento.',
            icon: 'success',
            heightAuto: false
          });
        }).catch((error) => {
          Swal.fire('Error!', '¡Ha ocurrido un error al eliminar el evento!', 'error');
        });
      }
    })
  }

  generatePdf() {
    let margins = {
      top: 80,
      bottom: 15,
      left: 15,
      width: 175
    };

    let handleElement = {
      '#IDquenosemuestra': function (element, renderer) {
        return true;
      }
    };
    var id = document.getElementById("contenedor");
    var pdf = new jsPDF();
    var myImage = new Image();
    myImage.src = '../../../assets/img/logos.PNG';

    const head = [['Título', 'Descripción', 'Fecha inicio', 'Fecha fin', 'Hora', 'Sala']];
    var data = [];
    this.subeventList.forEach(element => {
      data.push([element.title, element.descrip, element.date, element.finishdate, element.time, element.sala]);
      //console.log(element);
    });
    myImage.onload = function () {
      pdf.setFontSize(25)
      pdf.setFont('helvetica')
      pdf.setFontType('bold')
      pdf.text(20, 73, 'Universidad Técnica Particular de Loja')
      pdf.setDrawColor(36, 113, 163);
      pdf.setLineWidth(1.5);
      pdf.line(10, 60, 200, 60);
      pdf.addImage(myImage, 'png', 43, 15, 120, 40);
      pdf.fromHTML(id,
        margins.left, // x coord
        margins.top, {
        // y coord
        width: margins.width,
        // max width of content on PDF
        'elementHandlers': handleElement
      },
        () => {
          // Crea otra pag
          pdf.addPage();
          pdf.setFontSize(20)
          pdf.setFont('helvetica')
          pdf.setFontType('bold')
          pdf.text(15, 25, 'Subeventos:')
          autoTable(pdf, {
            head: head,
            body: data,
            theme: 'grid',
            styles: {
              font: 'helvetica',
              fontStyle: 'normal'
            },
            margin: { top: 30 }
          })
          pdf.save("reporte.pdf");
        },
        margins
      );
    };
  }
}
