import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserI } from '../../../shared/models/user.interface';
import { AuthService } from './../../../shared/services/auth.service';
import { FileI } from './../../../shared/models/file.interface';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { UserService } from '../../users/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public image: FileI;
  public currentImage: string = 'https://www.tuplanweb.com/proyecto/Plantilla/img/user/edwin.jpg';
  public user$: Observable<UserI>;
  public idUser: string;
  constructor(private authSvc: AuthService, private usrService: UserService) { }


  public profileForm = new FormGroup({
    displayName: new FormControl('', Validators.required),
    email: new FormControl({ value: '', disable: true }, Validators.required),
    photoURL: new FormControl('', Validators.required),
    phoneNumber: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required)
  })
  ngOnInit() {
    this.authSvc.userData$.subscribe(user => {
      this.user$ = this.usrService.getUser(user.uid);
      this.user$.subscribe(us => {
        this.initValuesForm(us);
      })
      this.idUser = user.uid;
    });
  }
  onSaveUser(user: UserI): void {
    this.authSvc.preSaveProfile(user, this.image, this.idUser);
    Swal.fire({
      icon: 'success',
      title: 'Los cambios se han guardado correctamente',
      showConfirmButton: false,
      timer: 1500,
      heightAuto: false
    })
  }
  private initValuesForm(user: UserI) {
    if (user.photoURL) {
      this.currentImage = user.photoURL;
    }
    this.profileForm.patchValue({
      displayName: user.displayName,
      email: user.email,
      phoneNumber: user.phoneNumber,
      description: user.description
    })
  }
  handleImage(image: FileI): void {
    this.image = image;
  }
}
