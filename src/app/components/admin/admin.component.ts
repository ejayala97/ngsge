import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {  
  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  public opened="true";
  constructor(public authSvc: AuthService) { }
  sideBarOpen = true;
  ngOnInit() {
  }
  onLogout(): void{
    this.authSvc.logout();
  }
  sideBarToggler() {
    this.sideBarOpen = !this.sideBarOpen;
  }
}
