import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListUsersRoutingModule } from './list-users-routing.module';
import { ListUsersComponent } from './list-users.component';
import { MaterialModule } from 'src/app/material.module';


@NgModule({
  declarations: [ListUsersComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ListUsersRoutingModule
  ]
})
export class ListUsersModule { }
