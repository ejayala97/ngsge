import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { UserService } from '../user.service';
import { Observable } from 'rxjs';
import { UserI } from 'src/app/shared/models/user.interface';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

  public users$: Observable<UserI[]>;
  public userList: UserI[] = [];

  displayedColumns: string[] = ['UID', 'displayName', 'email'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private svc: UserService) { }

  ngOnInit() {
    this.users$ =  this.svc.getAllUsers();
    const subscribe = this.users$.subscribe(users => {
      users.forEach(user =>{
        console.log(user);
        if(!user.role){
          const ObjUser = {
            email: user.email,
            //lastname: user.lastname,
            //name: user.name,
            displayName: user.displayName,
            uid: user.uid
          }
          this.userList.push(ObjUser as UserI);
        }
        this.dataSource.data = this.userList;
        subscribe.unsubscribe();
      })
    })
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(user: UserI) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
