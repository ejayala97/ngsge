import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { UserI } from 'src/app/shared/models/user.interface';
import { map, reduce } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { User } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private usersCollection: AngularFirestoreCollection<UserI>;

  constructor(private afs: AngularFirestore) { 
    this.usersCollection = this.afs.collection<UserI>('users');
  }

  public getAllUsers(): Observable<UserI[]> {
    return this.usersCollection
      .snapshotChanges()
      .pipe(
        map(actions =>
          actions.map(a => {
            const data = a.payload.doc.data() as UserI;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      )
  }
  public updateUserProfile(usr: UserI, id: string){
    console.log('usuario actualizado', id);
    return this.usersCollection.doc(id).update(usr);
  }
  public getOneUser(id: UserI): Observable<UserI> {
    // Create a query against the collection.
    return this.afs.doc<UserI>(`users/${id}`).valueChanges();
  }
  public getUser(id: string): Observable<UserI> {
    // Create a query against the collection.
    return this.afs.doc<UserI>(`users/${id}`).valueChanges();
  }
  public getadminUser(id: UserI): Observable<UserI> {
    // Create a query against the collection.
    console.log('llego aqui');
    /*var query = this.afs.collection('users', 
      ref => ref.where("mail", "==", id.email));
    query.valueChanges(val => console.log(val));*/
    return this.afs.doc<UserI>(`users/${id}`).valueChanges();
  }
}
